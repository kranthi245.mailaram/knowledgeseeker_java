package com.example.knowledgeseeker_java;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class SearchList extends ArrayAdapter<Subject> {


    private Activity context;
    private List<Subject> searchList;
    private DatabaseReference myDatabase;


    public SearchList(Activity context, List<Subject> searchList){
        super(context,R.layout.adapter_search_layout,searchList);

        this.context = context;
        this.searchList = searchList;

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();


        View searchListViewItem = inflater.inflate(R.layout.adapter_search_layout, null, true);

        final TextView SubjectName = (TextView) searchListViewItem.findViewById(R.id.subject_searchView);
        TextView TeachingTime = (TextView) searchListViewItem.findViewById(R.id.TeachingTime_searchview);
        TextView Price = (TextView) searchListViewItem.findViewById(R.id.price_searchview);
        TextView PhoneNr= (TextView) searchListViewItem.findViewById(R.id.Phone_Nr_searchview);
        TextView Tutor = (TextView) searchListViewItem.findViewById(R.id.tutorName_searchview);


        Subject oneSubject = searchList.get(position);

        SubjectName.setText(oneSubject.getSubjectName());
        TeachingTime.setText(oneSubject.getTeachingTime());
        Price.setText(oneSubject.getPricePerHour());
        PhoneNr.setText(oneSubject.getPhoneNr());
        Tutor.setText(oneSubject.getTutorName());



        return searchListViewItem;
    }
}
