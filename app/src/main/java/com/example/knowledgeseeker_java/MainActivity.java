package com.example.knowledgeseeker_java;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button SignInButton;
    private Button LogButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_main);

        SignInButton = (Button) findViewById(R.id.SignUpButton);
        SignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSignUpActivity();
            }
        });


        LogButton = (Button) findViewById(R.id.LogInButton);
        LogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchLogInActivity();
            }
        });
    }

    public void launchSignUpActivity(){

        Intent intent = new Intent(this,SignUp.class);

        startActivity(intent);

    }

    public void launchLogInActivity(){

        Intent intent = new Intent(this,Login.class);

        startActivity(intent);

    }
}
