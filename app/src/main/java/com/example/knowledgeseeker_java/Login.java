package com.example.knowledgeseeker_java;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import org.json.JSONObject;

import java.util.Iterator;


public class Login extends AppCompatActivity {

    private FirebaseAuth myFirebaseAuth;
    private Button LoginButton;
    private EditText LoginEmail;
    private EditText LoginPass;
    private DatabaseReference myDatabase;

    private JSONObject json;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_login);

        LoginButton = (Button) findViewById(R.id.LogInButtonFromLogScreen);
        LoginEmail = (EditText) findViewById(R.id.LoginEmailFromLogScreen);
        LoginPass = (EditText) findViewById(R.id.LoginPasswordFromLogScreen);

        myFirebaseAuth = FirebaseAuth.getInstance();
        /*
        FirebaseAuth.getInstance().getCurrentUser().getUid();*/

       myDatabase = FirebaseDatabase.getInstance().getReference();



        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myFirebaseAuth.signInWithEmailAndPassword(
                        LoginEmail.getText().toString(),
                        LoginPass.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    ScreenSelect_from_userType(myDatabase);
                                }else{
                                    Toast.makeText(Login.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });
    }


    protected void ScreenSelect_from_userType(DatabaseReference ref){


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.child("Tutors").getChildren()){
                    if(snapshot.getKey().equals(myFirebaseAuth.getCurrentUser().getUid())){
                        startActivity(new Intent(Login.this,SubjectScreen.class));

                    }

                }

                for(DataSnapshot snapshot : dataSnapshot.child("Students").getChildren()){
                    if(snapshot.getKey().equals(myFirebaseAuth.getCurrentUser().getUid())){
                        startActivity(new Intent(Login.this,ShowSubjectsToStudents.class));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
