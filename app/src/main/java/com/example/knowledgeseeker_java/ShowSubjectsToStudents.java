package com.example.knowledgeseeker_java;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import android.widget.ListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;

import java.util.ArrayList;
import java.util.List;


public class ShowSubjectsToStudents extends AppCompatActivity {

    private FirebaseUser user;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference ref;

    ListView mySearchedSubjectListView;
    List<Subject> searchList;
    static String test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_show_subjects_to_students);



        user =  FirebaseAuth.getInstance().getCurrentUser();


        ref = FirebaseDatabase.getInstance().getReference("Subjects");
        mySearchedSubjectListView = (ListView) findViewById(R.id.search_list);
        searchList = new ArrayList<>();
    }


    @Override
    protected void onStart() {
        super.onStart();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                searchList.clear();
                for(DataSnapshot subjectSnapshot: dataSnapshot.getChildren()){

                    for(DataSnapshot subSnapshot : subjectSnapshot.getChildren()){

                            Subject oneSubject = subSnapshot.getValue(Subject.class);

                            searchList.add(oneSubject);


                        }
                }

                SearchList adapter = new SearchList(ShowSubjectsToStudents.this, searchList);
                mySearchedSubjectListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
