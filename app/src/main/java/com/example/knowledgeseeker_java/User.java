package com.example.knowledgeseeker_java;

public class User {

    private String firstName, lastName,email;

    public User(){

    }

    public User(String firstName, String lastName, String email){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getfirstName(){
        return this.firstName;
    }

    public String getlastName(){
        return this.lastName;
    }

    public String getemail(){
        return this.email;
    }






    public void setfirstName(String firstName){
        this.firstName = firstName;
    }

    public void setlastName(String lastName){
        this.lastName = lastName;
    }

    public void setemail(String email){
        this.email = email;
    }

}
