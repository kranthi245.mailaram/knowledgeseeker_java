package com.example.knowledgeseeker_java;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class SubjectScreen extends AppCompatActivity {

    private FirebaseUser user;
    private Button addSubjectsButton;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference ref;

    ListView mySubjectListView;
    List<Subject> subjectList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_subject_screen);

        user =  FirebaseAuth.getInstance().getCurrentUser();
        addSubjectsButton= (Button) findViewById(R.id.AddSubjectsButton);

        ref = FirebaseDatabase.getInstance().getReference("Subjects");
        mySubjectListView = (ListView) findViewById(R.id.listview);
        subjectList = new ArrayList<>();

        addSubjectsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchAddSubjectActivity();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                subjectList.clear();
                for(DataSnapshot subjectSnapshot: dataSnapshot.getChildren()){

                    if((FirebaseAuth.getInstance().getCurrentUser().getUid()).equals(subjectSnapshot.getKey())){
                        for(DataSnapshot subSnapshot : subjectSnapshot.getChildren()){

                            Subject oneSubject = subSnapshot.getValue(Subject.class);
                            subjectList.add(oneSubject);
                        }
                    }



                }

                Subject_List adapter = new Subject_List(SubjectScreen.this, subjectList);
                mySubjectListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void launchAddSubjectActivity(){

        Intent intent = new Intent(this,addSubject.class);

        startActivity(intent);

    }
}

