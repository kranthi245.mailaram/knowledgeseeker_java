package com.example.knowledgeseeker_java;

public class Subject {

    private String SubjectName, TeachingTime, Price, PhoneNr, TutorName;

    public Subject(){

    }



    public Subject(String PhoneNr, String Price, String SubjectName, String TeachingTime, String TutorName){

        this.SubjectName = SubjectName;
        this.TeachingTime = TeachingTime;
        this.Price = Price;
        this.PhoneNr = PhoneNr;
        this.TutorName  = TutorName;
    }

    public String getSubjectName(){
        return this.SubjectName;
    }

    public String getTeachingTime(){
        return this.TeachingTime;
    }

    public String getPricePerHour(){
        return this.Price;
    }

    public String getPhoneNr(){
        return this.PhoneNr;
    }

    public String getTutorName(){
        return this.TutorName;
    }




    public void setSubjectName(String SubjectName){
        this.SubjectName = SubjectName;
    }

    public void setTeachingTime(String TeachingTime){
        this.TeachingTime = TeachingTime;
    }

    public void setPricePerHour(String Price){
        this.Price = Price;
    }

    public void setPhoneNr(String PhoneNr){
        this.PhoneNr = PhoneNr;
    }

    public void setTutorName(String TutorName){
        this.TutorName = TutorName;
    }
}
