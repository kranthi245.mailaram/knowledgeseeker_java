package com.example.knowledgeseeker_java;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUp extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth myFirebaseAuth;
    private DatabaseReference myDatabase;
    private EditText firstName;
    private EditText lastName;
    private EditText eMail;
    private EditText password;
    private EditText repeatedPassword;
    private Button signUpButton;
    private Switch TutorAcc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_sign_up);

        firstName = (EditText) findViewById(R.id.FirstName_SignUpScreen);
        lastName =(EditText) findViewById(R.id.LastName_SignUpScreen);
        eMail = (EditText) findViewById(R.id.Email_SignUpScreen);
        password = (EditText) findViewById(R.id.Password_SignUpScreen);
        repeatedPassword = (EditText) findViewById(R.id.RepeatPassword_SignUpScreen);
        signUpButton = (Button) findViewById(R.id.SignUpButton_SignUpScreen);
        TutorAcc = (Switch) findViewById(R.id.Switch_SignUpScreen);


        myFirebaseAuth = FirebaseAuth.getInstance();

        signUpButton.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {

        if(view == signUpButton){

            registerUser();
        }
    }

    private void registerUser(){

        final String firstname = firstName.getText().toString().trim();
        final String lastname = lastName.getText().toString().trim();
        final String email = eMail.getText().toString().trim();
        String pass = password.getText().toString().trim();
        String repeatedPass = repeatedPassword.getText().toString().trim();


        System.out.println("Entered register;");
        if(TextUtils.isEmpty(firstname)){
            Toast.makeText(this, "Please enter firstName", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(lastname)){
            Toast.makeText(this, "Please enter lastName", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(repeatedPass)){
            Toast.makeText(this, "Please repeat the password", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!(pass.equals(repeatedPass))){
            Toast.makeText(this, "Password and repeated password are not same", Toast.LENGTH_LONG).show();
            return;
        }


        myFirebaseAuth.createUserWithEmailAndPassword(email,pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            User user = new User(
                                    firstname,
                                    lastname,
                                    email
                            );

                            if(TutorAcc.isChecked()){

                                myDatabase = FirebaseDatabase.getInstance().getReference("Tutors");
                            }else{
                                myDatabase = FirebaseDatabase.getInstance().getReference("Students");
                            }


                            myDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                      .setValue(user)
                                      .addOnCompleteListener(new OnCompleteListener<Void>() {
                                          @Override
                                          public void onComplete(@NonNull Task<Void> task1) {

                                              if(task1.isSuccessful()){
                                                    if(TutorAcc.isChecked()){
                                                        startActivity(new Intent(SignUp.this,SubjectScreen.class));
                                                    }else{
                                                        startActivity(new Intent(SignUp.this,ShowSubjectsToStudents.class));
                                                    }

                                              }else{
                                                  Toast.makeText(SignUp.this, task1.getException().getMessage(), Toast.LENGTH_LONG).show();

                                              }

                                          }
                                      });



                        }else{
                            Toast.makeText(SignUp.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
