package com.example.knowledgeseeker_java;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class Subject_List extends ArrayAdapter<Subject> {

    private Activity context;
    private List<Subject> subjectList;
    private DatabaseReference myDatabase;


    public Subject_List(Activity context, List<Subject> subjectList){
    super(context,R.layout.adapter_view_layout,subjectList);
    this.context = context;
    this.subjectList = subjectList;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();


        View listViewItem = inflater.inflate(R.layout.adapter_view_layout, null, true);

        final TextView SubjectName = (TextView) listViewItem.findViewById(R.id.subject_adapterview);
        TextView TeachingTime = (TextView) listViewItem.findViewById(R.id.teachingTime_adapterview);
        TextView Price = (TextView) listViewItem.findViewById(R.id.price_adapterview);
        TextView PhoneNr= (TextView) listViewItem.findViewById(R.id.Tel_adapterview);
        Button button = (Button) listViewItem.findViewById(R.id.delete_btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDatabase = FirebaseDatabase.getInstance().getReference("Subjects");
                myDatabase
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child(SubjectName.getText().toString()).removeValue();

                //subjectList.remove(position);
                //notifyDataSetChanged();
            }
        });

        Subject oneSubject = subjectList.get(position);

        SubjectName.setText(oneSubject.getSubjectName());
        TeachingTime.setText(oneSubject.getTeachingTime());
        Price.setText(oneSubject.getPricePerHour());
        PhoneNr.setText(oneSubject.getPhoneNr());

        return listViewItem;

    }
}
