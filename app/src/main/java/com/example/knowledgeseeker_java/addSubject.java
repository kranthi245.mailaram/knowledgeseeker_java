package com.example.knowledgeseeker_java;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.*;

import java.util.ArrayList;
import java.util.List;

public class addSubject extends AppCompatActivity implements View.OnClickListener {

    private EditText subjectName;
    private EditText teachingTime;
    private EditText pricePerHour;
    private EditText phoneNumber;
    private Button cancelButton;
    private Button saveButton;
    private DatabaseReference myDatabase;
    private DatabaseReference myTutorDatabase;
    List<User> tutorUser;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_add_subject);

        subjectName = (EditText) findViewById(R.id.SubjectName_AddSubject);
        teachingTime = (EditText) findViewById(R.id.TeachingTime_AddSubject);
        pricePerHour = (EditText) findViewById(R.id.PricePerHour_AddSubject);
        phoneNumber = (EditText) findViewById(R.id.PhoneNumber_AddSubject);
        cancelButton = (Button) findViewById(R.id.CancelButton_AddSubject);
        saveButton = (Button) findViewById(R.id.SaveButton_AddSubject);

        myTutorDatabase = FirebaseDatabase.getInstance().getReference("Tutors");
        tutorUser = new ArrayList<>();
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);


        myTutorDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot) {


                for (DataSnapshot tutorData : dataSnapshot.getChildren()){


                    if(tutorData.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){

                        User tutor = tutorData.getValue(User.class);
                        tutorUser.add(tutor);

                    }

                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == saveButton){

            saveDataToFireBase();
        }

        if(view == cancelButton){

            startActivity(new Intent(addSubject.this,SubjectScreen.class));

        }

    }

    private void saveDataToFireBase(){

        String SubjectName = subjectName.getText().toString().trim();
        String TeachingTime = teachingTime.getText().toString().trim();
        String Price = pricePerHour.getText().toString().trim();
        String PhoneNr = phoneNumber.getText().toString().trim();




        if(TextUtils.isEmpty(SubjectName)){
            Toast.makeText(this, "Please enter the Subject Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(TeachingTime)){
            Toast.makeText(this, "Please enter the teachning time", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(Price)){
            Toast.makeText(this, "Please enter the price", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(PhoneNr)){
            Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_SHORT).show();
            return;
        }





        myDatabase = FirebaseDatabase.getInstance().getReference("Subjects");




        Subject SubjectObject = new Subject(PhoneNr,Price,SubjectName,TeachingTime,tutorUser.get(0).getfirstName());





        myDatabase.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(SubjectName)
                .setValue(SubjectObject)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task1) {

                        if(task1.isSuccessful()){
                            startActivity(new Intent(addSubject.this,SubjectScreen.class));

                        }else{
                            Toast.makeText(addSubject.this, task1.getException().getMessage(), Toast.LENGTH_LONG).show();

                        }

                    }
                });

    }
}
